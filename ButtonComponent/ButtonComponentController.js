({
	handleClick : function(component, event, helper) {
        document.getElementById('modalContainer').style.removeProperty('display');
	},
    cancel : function(component, event, helper) {
        document.getElementById('modalContainer').style.display = 'none';
        document.getElementById('text').innerHTML = "You clicked the 'Cancel' button";
	},
 	confirm : function(component, event, helper) {
        document.getElementById('modalContainer').style.display = 'none';
        document.getElementById('text').innerHTML = "You clicked the 'Confirm' button";
	}
})